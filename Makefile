
all:
	@echo "Ne fais rien pour éviter de tout compiler par inadvertance\nUtilisez \"make main\" pour compiler le complet"

classement:
	@gcc -c -g ./src/classement.c -o ./build/classement.o

gestionPartie:
	@gcc -c -g ./src/gestionPartie.c -o ./build/gestionPartie.o -lwiringPi -L. -lu8g2

gestionJoueur:
	@gcc -c -g ./src/gestionJoueur.c -o ./build/gestionJoueur.o

gestionEnnemis:
	@gcc -c -g ./src/gestionEnnemis.c -o ./build/gestionEnnemis.o

gestionMissiles:
	@gcc -c -g ./src/gestionMissiles.c -o ./build/gestionMissiles.o

gestionEcran:
	@gcc -c -g ./src/gestionEcran.c -o ./build/gestionEcran.o -lwiringPi -L. -lu8g2

gestionSHIELDIUT:
	@gcc -c -g ./src/gestionSHIELDIUT.c -o ./build/gestionSHIELDIUT.o -lwiringPi


#On lie le tout
main: gestionPartie gestionJoueur gestionEnnemis gestionMissiles gestionEcran gestionSHIELDIUT classement # Compile le complet
	@gcc main.c ./build/*.o -o ./SpaceInvader -lwiringPi -lm -L. -lu8g2
	@chmod 755 ./SpaceInvader
	@echo "Normalement tout s'est bien passé vous trouverez le programme compilé içi: ./SpaceInvader"

clean:
	@rm -r -f ./build/
	@mkdir ./build
	@rm -f SpaceInvader
	@clear
	@echo "Clean completed"
