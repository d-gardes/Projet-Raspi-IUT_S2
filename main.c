

/******************************************************************************************
*             _____                        _____                     _                    *
*            / ____|                      |_   _|                   | |                   *
*           | (___  _ __   __ _  ___ ___    | |  _ ____   ____ _  __| | ___ _ __          *
*            \___ \| '_ \ / _` |/ __/ _ \   | | | '_ \ \ / / _` |/ _` |/ _ \ '__|         *
*            ____) | |_) | (_| | (_|  __/  _| |_| | | \ V / (_| | (_| |  __/ |            *
*           |_____/| .__/ \__,_|\___\___| |_____|_| |_|\_/ \__,_|\__,_|\___|_|            *
*                  | |                                                                    *
*                  |_|               IUT EDITION                                          *
*                                                                                         *
*                                                                                         *
*   LE JEU SE DÉROULE SUR UN ÉCRAN LCD BRANCHÉ SUR LE PORT SPI DU RPi                     *
*                     (st7920 128*64)                                                     *
*                                                                                         *
* lA GESTION DE CELUI CI EST GÉRÉ PAR LA LIBRAIRIE U8g2 QUE NOUS AVONS ADAPTÉE POUR LE PI *
*                           https://github.com/olikraus/u8g2                              *
*                                                                                         *
*******************************************************************************************/

#include "defConstantesEtTypes.h"

#include <stdio.h>
#include <stdlib.h>
//#include <malloc.h>

#include "./headers/classement.h"
#include "./headers/gestionPartie.h"
#include "./headers/gestionMissiles.h"
#include "./headers/gestionJoueur.h"
#include "./headers/gestionEnnemis.h"

#include "./headers/gestionEcran.h"
#include "./headers/gestionSHIELDIUT.h"


int menu(u8g2_t* u8g2); //cf plus bas



int main(int argc, char const *argv[]) {
  SHIELD_init();

  static u8g2_t u8g2;
  SCREEN_init(&u8g2);
  SCREEN_splashSreen(&u8g2);
  SHIELD_shutdown();

  int choix = 0;
  while (choix != 3) { // Tant qu'on demande pas de quitter
    choix = menu(&u8g2);
    delay(300); // Antirebond logiciel

    switch (choix) {
      case 0: // 1 jouer
        partie(&u8g2);
        //while (SHIELD_readBTNS() == AUTRE) { continue;}
        break;
      case 1: // 2 records
        u8g2_ClearDisplay(&u8g2);
        SCREEN_tableauDesScores(&u8g2);
        while (SHIELD_readBTNS() == AUTRE) { continue;}
        delay(300); //Antirebond
        break;
      case 2: // 3 crédits
        u8g2_ClearDisplay(&u8g2);
        SCREEN_credits(&u8g2);
        while (SHIELD_readBTNS() == AUTRE) { continue;}
        delay(300); //Antirebond
        break;
      default: // Normalement impossible
        break;
    }
  }

  // FIN DU PROGRAMME EXTINCTION EN RÈGLES:

  SCREEN_splashSreen(&u8g2);
  delay(120);

  SHIELD_shutdown();
  u8g2_ClearDisplay(&u8g2);
  u8g2_SetPowerSave(&u8g2, 1);
  return 0;
}

//************************************  MENU  ****************************************

int menu(u8g2_t* u8g2) { // 0 jouer, 1 records, 2 crédits, 3 quitter
  int choix = 0;

  while (SHIELD_readBTNS() != DROITE) {

    u8g2_ClearBuffer(u8g2);
    SCREEN_menu(u8g2);

    switch (choix) { //6*10
      case 0:
        u8g2_DrawBox(u8g2, 48, 19, 32, 9);
        u8g2_SetDrawColor(u8g2, 0); // bleu
        u8g2_DrawStr(u8g2, 49, 27, "Jouer");
        break;
      case 1:
        u8g2_DrawBox(u8g2, 15, 31, 98, 9);
        u8g2_SetDrawColor(u8g2, 0); // bleu
        u8g2_DrawStr(u8g2, 16, 39, "Meilleurs scores");
        break;
      case 2:
        u8g2_DrawBox(u8g2, 42, 43, 44, 9);
        u8g2_SetDrawColor(u8g2, 0); // bleu
        u8g2_DrawUTF8(u8g2, 43, 51, "Crédits");
        break;
      case 3:
        u8g2_DrawBox(u8g2, 42, 55, 44, 9);
        u8g2_SetDrawColor(u8g2, 0); // bleu
        u8g2_DrawStr(u8g2, 43, 63, "Quitter");
        break;
    }
    u8g2_SendBuffer(u8g2);
    Direction btns = AUTRE;
    do {
      btns = SHIELD_readBTNS();
      if (btns == GAUCHE) choix = (choix + 1) % 4; // si ++ ou +=1 modulo inutile
    } while(btns == AUTRE);
  }
  return choix;
}
