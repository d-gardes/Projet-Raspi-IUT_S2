#include "../defConstantesEtTypes.h"


#include <errno.h>
#include <stdint.h>
#include <unistd.h>
#include <time.h>
#include <math.h>

#include <stdio.h>
#include <stdlib.h>

#include "u8g2.h"
#include <wiringPi.h>
#include <wiringPiSPI.h>

#include "gestionSHIELDIUT.h"
#include "classement.h"
#include <string.h>


/* //Defining ports (BCM pins) for SPI communication for the Raspberry Pi.
#define LCD_CS		8	// LCD chip select.
#define SPI_MOSI	10	// SPI MOSI pin.
#define SPI_CLK		11	// SPI CLK pin.
#define RESET		2 */


// TOUTES LES METHODES ICI SONT PREFIXES DE "SCREEN_"

void SCREEN_init(u8g2_t* u8g2);

// Screens de menu / acceuils etc ...

void SCREEN_splashSreen(u8g2_t* u8g2); // écran de boot

void SCREEN_menu(u8g2_t* u8g2); // jouer, tableau des scores, crédits, quitter

void SCREEN_tableauDesScores(u8g2_t* u8g2);

void SCREEN_credits(u8g2_t* u8g2); // print nos noms

// Différents prints pour générer l'écran en cours de partie.

void SCREEN_printScore(u8g2_t* u8g2, int score);

void SCREEN_printEnnemis(u8g2_t* u8g2, Ennemi tabEnnemis[], int nbEnnemis); //OK

void SCREEN_printJoueur(u8g2_t* u8g2, Joueur joueur);

void SCREEN_printMissiles(u8g2_t* u8g2, Missile tabMissiles[], int nbMissiles);

void SCREEN_printExplosion(u8g2_t* u8g2, Position position);

// Fins de parties

void SCREEN_gameOver(u8g2_t* u8g2, unsigned int score);

void SCREEN_gameWin(u8g2_t* u8g2, unsigned int score);

void SCREEN_nouveauRecord(u8g2_t* u8g2);

// A INTEGERER DANS LA BIBLI U8G2

uint8_t u8x8_byte_rpi_hw_spi(u8x8_t *u8x8, uint8_t msg, uint8_t arg_int, void *arg_ptr);

uint8_t u8x8_rpi_gpio_and_delay(u8x8_t *u8x8, uint8_t msg, uint8_t arg_int, void *arg_ptr);
