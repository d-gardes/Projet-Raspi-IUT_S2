#include "../defConstantesEtTypes.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "u8g2.h"


void creerFichier();

void ecrireRecords(Score tabScores[NB_RECORDS]);

void lireRecords(Score* tabScores[NB_RECORDS]);

void checkNouveauRecord(u8g2_t* u8g2, Score nouveauScore);