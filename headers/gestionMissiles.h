#include "../defConstantesEtTypes.h"

#include "u8g2.h"
#include <stdlib.h>

// Fonctions tirer... prépare le missile pour l'ajouter au tableau avec nouveauMissile()
Missile tirerEnnemi(Ennemi tabEnnemis[], int nbEnnemis);

Missile tirerJoueur(Joueur joueur);

void nouveauMissile(Missile* tabMissiles[], int* nbMissiles, Missile nouveauMissile);

void retirerMissile(Missile* tabMissiles[], int* nbMissiles, int indiceARetirer);

void deplacerMissiles(Missile* tabMissiles[], int nbMissiles); // Déplace tous les missiles

void checkColisions(u8g2_t* u8g2, Missile* tabMissiles[], int* nbMissiles, Ennemi* tabEnnemis[], int* nbEnnemis, Joueur* joueur);
// retire les missiles, ennemis, vies si colisions
