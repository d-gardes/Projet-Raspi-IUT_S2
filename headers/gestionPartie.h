#include "../defConstantesEtTypes.h"

#include "gestionEnnemis.h"
#include "gestionJoueur.h"
#include "gestionMissiles.h"
#include "gestionEcran.h"
#include "classement.h"
#include "u8g2.h"

void partie(u8g2_t* u8g2);
