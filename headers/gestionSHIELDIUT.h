#include "../defConstantesEtTypes.h"

#include <wiringPi.h>
#include <math.h>
#include <time.h>
#include <softPwm.h>


// TOUTES LES METHODES ICI SONT PREFIXES DE "SHIELD_"
void SHIELD_init();

void SHIELD_finPartie(); //ancien sinBargraph

void SHIELD_printVies(Joueur joueur);

Direction SHIELD_readBTNS();

void SHIELD_shutdown();