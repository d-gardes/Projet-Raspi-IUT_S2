#include "../defConstantesEtTypes.h"


void descendreEnnemis(Ennemi* tabEnnemis[], int nbEnnemis, int valeurDescente);

void testchangementDirectionEnnemis(Ennemi* tabEnnemis[], Direction* sensEnnemis, int nbEnnemis);

void deplacerEnnemis(Ennemi* tabEnnemis[], Direction* sensEnnemis, int nbEnnemis); //déplace tous les ennemis

void spawnEnnemis(Ennemi* tabEnnemis[], int* nbEnnemis, int nbPointsVie); // remplis l'écran d'ennemis

void spawnBOSS(Ennemi* tabEnnemis[], int* nbEnnemis); // Niveau final

void retirerEnnemi(Ennemi* tabEnnemis[], int* nbEnnemis, int indiceARetirer);
