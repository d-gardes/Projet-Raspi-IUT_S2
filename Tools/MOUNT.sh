#!/bin/bash

echo "Montage du RaspberryPi"
echo "Dernier octet de l'IP du rasp: "
read lastoct

mkdir ../rpi
sshfs pi@10.70.2.$lastoct:/home/pi/Documents `pwd`/../rpi

cp ../headers/ ../src/ ../libu8g2.a ../main.c ../defConstantesEtTypes.h ../Makefile  ../rpi/ -f -r
mkdir ../rpi/build/

echo "Monté ! dans"`pwd`"/../rpi/"
