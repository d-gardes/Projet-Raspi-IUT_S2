#!/bin/bash

echo "Démontage du RaspberryPi"

rm -r -f ../rpi/build/
rm ../rpi/SpaceInvader

cp ../rpi/* .. -f -r

rm ../rpi/* -f -r

sleep 2
fusermount -u `pwd`/../rpi

rmdir ../rpi

echo "Démonté !"
