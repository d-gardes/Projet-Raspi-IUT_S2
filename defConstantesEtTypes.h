
#ifndef CONSTANTES_ET_TYPES // On évite de define 50 fois la même chose
#define CONSTANTES_ET_TYPES

// Ici on déclare toutes les constantes (define) et les types perso

typedef struct {
  unsigned int pos_x;
  unsigned int pos_y;
} Position;

typedef enum { DROITE, GAUCHE, AUTRE} Direction;
// AUTRE = indéfini (par ex pas de btn pressé ou les 2 en mm temps)
// AUTRE IMPOSSIBLE POUR MISSILES

// PARTIE MISSILES ********************************************************
typedef enum { HOSTILE, ALLIE} SensMissile;
#define VITESSE_MISSILES 3
typedef struct Missile {
  SensMissile sens;
  Position position;
} Missile;


// PARTIE JOUEUR **********************************************************
#define JOUEUR_POS_Y SCREEN_EIGHT-JOUEUR_EIGHT-1
typedef struct Joueur {
    unsigned int pos_x;
    int ptsVie;
} Joueur;

// PARTIE ENNEMIS *********************************************************
#define NB_PTS_VIE_BOSS 7
typedef struct Ennemi {
  int type;
  int ptsVie;
  Position position;
} Ennemi;

// PARTIE CLASSEMENT ******************************************************
#define NB_RECORDS 5
#define TAILLE_MAX_NOM 10

typedef struct Score {
  char nom[TAILLE_MAX_NOM+1];
  unsigned int score;
} Score;


// PARTIE ECRAN **********************************************************
/* Defining ports (BCM pins) for SPI communication for the Raspberry Pi. */
#define LCD_CS		8	/* LCD chip select. */
#define SPI_MOSI	10	/* SPI MOSI pin. */
#define SPI_CLK		11	/* SPI CLK pin. */
#define RESET		2

#define SCREEN_WIDTH 128
#define SCREEN_EIGHT 64

#define ALIEN_WIDTH 8
        #define ALIEN_EIGHT 8
#define JOUEUR_WIDTH 9
        #define JOUEUR_EIGHT 6
#define BOSS_WIDTH 12
        #define BOSS_EIGHT 8
#define MISSILE_WIDTH 3
        #define MISSILE_EIGHT 7
#define EXPLOSION_WIDTH 9
        #define EXPLOSION_EIGHT 7

#define TITLE_WIDTH 88
        #define TITLE_EIGHT 41
#define ALIEN_TITLE_WIDTH 11
        #define ALIEN_TITLE_EIGHT 8
//Textures dans .h correspondant


// PARTIE SHIELD IUT ******************************************************

#define BTN_G 21
#define BTN_D 5  // ATTENTION NOTATION BCM
#define LED_V 12
#define LED_R 6
// le bargraph est dans le .h correspondant

#endif
