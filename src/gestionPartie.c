#include "../defConstantesEtTypes.h"

#include "../headers/gestionEnnemis.h"
#include "../headers/gestionJoueur.h"
#include "../headers/gestionMissiles.h"
#include "../headers/gestionEcran.h"
#include "../headers/gestionSHIELDIUT.h"
#include "../headers/classement.h"
#include "../headers/u8g2.h"

#include <stdlib.h>
#include <malloc.h>

void partie(u8g2_t* u8g2) {

  /* ***************** On setup le necessaire pour jouer ******************** */

  int score = 0;
  Joueur joueur = { (SCREEN_WIDTH-JOUEUR_WIDTH)/2, 8};

  Ennemi* tabEnnemis = NULL;
  int nbEnnemis = 0;
  Direction sensEnnemis;

  Missile* tabMissiles = NULL;
  int nbMissiles = 0;

  /* ********************** INIT DONE *************************************** */

  for (int level = 0; level < 3; level++) {
    score = 5;
    break;

    // Setup niveau
    if (level < 2 ) {spawnEnnemis(&tabEnnemis, &nbEnnemis, level+1); }// aliens simples
    else {spawnBOSS(&tabEnnemis, &nbEnnemis); } // BOSS final
    sensEnnemis = DROITE;

    joueur.ptsVie = 8;

    // Décompte 3,2,1 ?

    while (nbEnnemis > 0 && joueur.ptsVie > 0) { // Tant que niveau non terminé et joueur vivant

      // ***********Phase 0: Reset des leds

        digitalWrite(LED_R, LOW);
        digitalWrite(LED_V, LOW);

      // ***********Phase 1: Déplacements, destructions et bordels divers

      //Déplacements
      if (joueur.ptsVie > 0) deplacerJoueur(&joueur, SHIELD_readBTNS());
      deplacerEnnemis(&tabEnnemis, &sensEnnemis, nbEnnemis);
      deplacerMissiles(&tabMissiles, nbMissiles);

      //Destructions
      checkColisions(u8g2, &tabMissiles, &nbMissiles, &tabEnnemis, &nbEnnemis, &joueur); // Gère les destructions comme un grand

      //Tirs
      if ((rand() % 5) == 1) nouveauMissile( &tabMissiles, &nbMissiles, tirerJoueur(joueur)); // Tirs joueur
      if (nbEnnemis > 0) { // On évite les modulo 0
        if (level != 2) {
          if ((rand() % 3) == 1) nouveauMissile( &tabMissiles, &nbMissiles, tirerEnnemi(tabEnnemis, nbEnnemis)); // Tirs BOSS
        } else if ((rand() % 5) == 1) nouveauMissile( &tabMissiles, &nbMissiles, tirerEnnemi(tabEnnemis, nbEnnemis)); // Tirs Ennemis
      }

      //Ajout des scores
      score+= 2;

      // ***********Phase 2 Affichage (de l'arrière au premier plan)
      // NOTE IMPORTANTE : Les explosions sont print depuis checkColisions pour éviter de parcourir le tableau 2 fois

      SCREEN_printMissiles(u8g2, tabMissiles, nbMissiles);
      SCREEN_printJoueur(u8g2, joueur);
      SCREEN_printEnnemis(u8g2, tabEnnemis, nbEnnemis);
      SCREEN_printScore(u8g2, score);
      SHIELD_printVies(joueur);

      u8g2_SendBuffer(u8g2); // Envoi de l'image à l'écran
      u8g2_ClearBuffer(u8g2); // Vidage du buffer image (l'écran ne change pas)
    }

    //clean niveau
    free(tabEnnemis);
    tabEnnemis = NULL;
    nbEnnemis = 0;
    free(tabMissiles);
    tabMissiles = NULL;
    nbMissiles = 0;

    if (joueur.ptsVie <= 0) break; // Force la sortie si perdu

  }

  SHIELD_shutdown();
  if (joueur.ptsVie > 0) SCREEN_gameWin(u8g2, score);
  else SCREEN_gameOver(u8g2, score);
  SHIELD_finPartie();
  delay(25);
  SHIELD_shutdown();

  checkNouveauRecord(u8g2, (Score) {"", score});

  /* ******** Fin de partie mémoire vidée plus haut. Tout est OK ***************** */
} // retour au menu