#include "../defConstantesEtTypes.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "../headers/u8g2.h"
#include "../headers/gestionEcran.h"

void creerFichier(){
  char tableauVide[TAILLE_MAX_NOM * 4 - 1] = ""; //init vide
  for (int i = 0; i < TAILLE_MAX_NOM; i++) {
    strcat(tableauVide, "X:0\n");
  }
  FILE* fic;
	fic = fopen("tableau_score.si", "w+");
  fprintf(fic, tableauVide);
  fclose(fic);
}

void ecrireRecords(Score tabScores[NB_RECORDS]) {

  FILE* fic;
	fic = fopen("tableau_score.si", "w+");
  for (int i = 0; i < NB_RECORDS; i++) {
    fprintf(fic, "%s:%d\n", tabScores[i].nom, tabScores[i].score);
  }
  fclose(fic);
}

void lireRecords(Score* tabScores[NB_RECORDS]){

  char* temp;

  //Ouverture fichier
  FILE* fic = NULL;
	fic = fopen("tableau_score.si", "r");
  if (fic == NULL) {
    creerFichier();
    fic = fopen("tableau_score.si", "r");
  }

  // Ouvert ou créé
  char* ligneEnCours = NULL;
  size_t tailleLigne = 0;

  int i = 0; // indice du record
  getline(&ligneEnCours, &tailleLigne, fic);
  for (int l = 0; l < NB_RECORDS; l++) { // parcours fichier ligne à ligne
    temp = strchr(ligneEnCours, ':');
    strncpy((*tabScores)[i].nom, ligneEnCours, (temp-ligneEnCours));
    (*tabScores)[i].nom[temp-ligneEnCours] = '\0';

    temp = (strchr(ligneEnCours, ':')+1);
    (*tabScores)[i].score = strtol(temp, NULL, 10);

    i++;
    getline(&ligneEnCours, &tailleLigne, fic);
  }
  free(ligneEnCours);
  fclose(fic);
}

void checkNouveauRecord(u8g2_t* u8g2, Score nouveauScore){

  Score tabScores[NB_RECORDS];
  Score* addrTab = tabScores;
  lireRecords(&addrTab);

  for (int i = 0; i < NB_RECORDS; i++) { // cherche place du score du joueur
    if (nouveauScore.score > tabScores[i].score) { // si place trouvée
      SCREEN_nouveauRecord(u8g2);
      printf("Bravo vous avez réalisé un nouveau record !\n Quel est votre nom ?\n" );
      scanf("%s", &(nouveauScore.nom));
      nouveauScore.nom[TAILLE_MAX_NOM] = '\0';
      printf("Bravo %s\n", nouveauScore.nom);
      for (int j = NB_RECORDS - 1;  j < i; j--) { // décale les scores pour le joueur
        tabScores[j] =tabScores[j--];
      }
      tabScores[i] = nouveauScore;
      break;
    }// else { /* Pas de record / }
  }
  ecrireRecords(tabScores);
}