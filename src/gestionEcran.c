#include "../defConstantesEtTypes.h"

#include <errno.h>
#include <stdint.h>
#include <unistd.h>
#include <time.h>
#include <math.h>
#include <string.h>

#include <stdio.h>
#include <stdlib.h>

#include "../headers/u8g2.h"
#include <wiringPi.h>
#include <wiringPiSPI.h>

#include "../headers/gestionSHIELDIUT.h"
#include "../headers/classement.h"


/* //Defining ports (BCM pins) for SPI communication for the Raspberry Pi.
#define LCD_CS		8	// LCD chip select.
#define SPI_MOSI	10	// SPI MOSI pin.
#define SPI_CLK		11	// SPI CLK pin.
#define RESET		2 */

// 1 dimension alien [1-3]
// 2 dimension sprite [1-2]
// 3 dimension data
const unsigned char textureAliens[3][2][8] = {
  {
    {0x18, 0x3C, 0x7E, 0xDB, 0xFF, 0x24, 0x5A, 0xA5 }, // alien 1_1
    {0x18, 0x3C, 0x7E, 0xDB, 0xFF, 0x24, 0x42, 0x24 }  // alien 1_2
  }, {
    {0x18, 0x7E, 0xFF, 0x99, 0xFF, 0x5A, 0x42, 0x3C }, // alien 2_1
    {0x18, 0x7E, 0xFF, 0x99, 0xFF, 0x24, 0x5A, 0x81 }  // alien 2_2
  }, {
    {0x24, 0x7E, 0xDB, 0xFF, 0xA5, 0x99, 0x81, 0xC3 }, // alien 3_1
    {0x24, 0x18, 0xBD, 0xDB, 0xBD, 0x99, 0x81, 0xC3 }  // alien 3_2
  }
};
const unsigned char textureBoss[2][16] = {
  { 0xF0, 0x00, 0xFE, 0x07, 0xFF, 0x0F, 0x67, 0x0E, 0xFF, 0x0F, 0x98, 0x01, 0x6C, 0x03, 0x03, 0x0C }, //boss 1
  { 0xF0, 0x00, 0xFE, 0x07, 0xFF, 0x0F, 0x67, 0x0E, 0xFF, 0x0F, 0x98, 0x01, 0x64, 0x02, 0x08, 0x01 } //boss 2
};

const unsigned char textureExplosion[14] = {0x45, 0x01, 0xAA, 0x00, 0x44, 0x00, 0x83, 0x01, 0x44, 0x00, 0xAA, 0x00, 0x45, 0x01 };

const unsigned char textureMissile[7] = {0x04, 0x02, 0x01, 0x02, 0x04, 0x02, 0x01 };

const unsigned char textureJoueur[12] = {0x10, 0x00, 0x38, 0x00, 0x38, 0x00, 0x7C, 0x00, 0xFF, 0x01, 0xFF, 0x01 };

const unsigned char title[] = { 0xfc, 0xe1, 0xff, 0x80, 0x0f, 0xf0, 0x87, 0xff, 0x07, 0xfc, 0xe1, 0xff, 0x80, 0x0f, 0xf0, 0x87, 0xff, 0x07, 0x87, 0xe7, 0xc1, 0xe3, 0x3d, 0x78, 0x9c, 0x03, 0x00, 0x87, 0xe7, 0xc1, 0xe3, 0x3d, 0x78, 0x9c, 0x03, 0x00, 0x07, 0xe0, 0xc1, 0x7b, 0xf0, 0x0e, 0x80, 0x03, 0x00, 0x07, 0xe0, 0xc1, 0x7b, 0xf0, 0x0e, 0x80, 0x03, 0x00, 0xfc, 0xe7, 0xc1, 0x7b, 0xf0, 0x0e, 0x80, 0xff, 0x00, 0xfc, 0xe7, 0xc1, 0x7b, 0xf0, 0x0e, 0x80, 0xff, 0x00, 0xfc, 0xe7, 0xc1, 0x7b, 0xf0, 0x0e, 0x80, 0xff, 0x00, 0x00, 0xee, 0xff, 0xf8, 0xff, 0x0e, 0x80, 0x03, 0x00, 0x00, 0xee, 0xff, 0xf8, 0xff, 0x0e, 0x80, 0x03, 0x00, 0x07, 0xee, 0x01, 0x78, 0xf0, 0x38, 0x9c, 0x03, 0x00, 0x0f, 0xee, 0x01, 0x78, 0xf0, 0x38, 0x9c, 0x07, 0x00, 0xfc, 0xe7, 0x01, 0x78, 0xf0, 0xf0, 0x87, 0xff, 0x07, 0xfc, 0xe7, 0x01, 0x78, 0xf0, 0xf0, 0x87, 0xff, 0x07, 0xfc, 0xc3, 0x00, 0x38, 0xf0, 0xe0, 0x87, 0xff, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7f, 0xe3, 0xc6, 0x78, 0xfc, 0xf8, 0xf7, 0xc7, 0x03, 0x7f, 0xe3, 0xc6, 0x78, 0xfc, 0xf8, 0xf7, 0xc7, 0x03, 0x1c, 0xe7, 0xc6, 0xfc, 0x8c, 0x19, 0x30, 0x6e, 0x06, 0x1c, 0xe7, 0xc6, 0xdc, 0x8c, 0x19, 0x30, 0x6e, 0x06, 0x1c, 0xef, 0xc6, 0x84, 0x0d, 0x1b, 0x30, 0x6e, 0x00, 0x1c, 0xef, 0xc6, 0x84, 0x0d, 0xfb, 0x30, 0x6e, 0x04, 0x1c, 0xff, 0xee, 0x84, 0x0d, 0xfb, 0x31, 0xce, 0x07, 0x1c, 0xff, 0xee, 0x84, 0x0d, 0xfb, 0x31, 0xce, 0x07, 0x1c, 0xfb, 0x7c, 0xfc, 0x0d, 0x1b, 0xf0, 0x01, 0x0c, 0x1c, 0xf3, 0x7c, 0xfc, 0x0d, 0x1b, 0xb0, 0x03, 0x08, 0x1c, 0xf3, 0x38, 0x84, 0x8d, 0x19, 0xb0, 0x77, 0x08, 0x1c, 0xf3, 0x38, 0x8c, 0x8d, 0x19, 0xb0, 0x77, 0x0c, 0x7f, 0xe3, 0x10, 0x84, 0xfd, 0xf8, 0x37, 0xce, 0x07, 0x7f, 0xe3, 0x10, 0x84, 0xfd, 0xf8, 0x03, 0xc0, 0x07 };

static unsigned char alien_title[2][16] = {
  { 0x04, 0x01, 0x88, 0x00, 0xFC, 0x01, 0x76, 0x03, 0xFF, 0x07, 0xFD, 0x05, 0x05, 0x05, 0xD8, 0x00 },
  { 0x04, 0x01, 0x89, 0x04, 0xFD, 0x05, 0x77, 0x07, 0xFF, 0x07, 0xFE, 0x03, 0x04, 0x01, 0x02, 0x02 }
};

// A INTEGERER DANS LA BIBLI U8G2

/**
 * U8G2 communication callback for the Raspberry Pi with wiringPi.
 */
uint8_t u8x8_byte_rpi_hw_spi(u8x8_t *u8x8, uint8_t msg, uint8_t arg_int, void *arg_ptr) {
	switch(msg)
	{
		case U8X8_MSG_BYTE_INIT:
			if(wiringPiSetup() == -1)
			{
				puts("WiringPi Error");
				exit(1);
			}

			if(wiringPiSPISetup(0, 100000) < 0)
			{
				printf("Unable to open SPI device 0: %s\n", strerror(errno));
				exit(1);
			}

			//puts("U8g2: Initialized Raspberry Pi SPI device.");
			break;

		case U8X8_MSG_BYTE_SET_DC:
			break;

		case U8X8_MSG_BYTE_START_TRANSFER:
			break;

		case U8X8_MSG_BYTE_SEND:
			wiringPiSPIDataRW(0, arg_ptr, arg_int);
			break;

		case U8X8_MSG_BYTE_END_TRANSFER:
			break;

		default:
			return 0;
	}

	return 1;
}

uint8_t u8x8_rpi_gpio_and_delay(u8x8_t *u8x8, uint8_t msg, uint8_t arg_int, void *arg_ptr) {
	static struct timespec tv = {0};
	switch(msg)
	{
		case U8X8_MSG_GPIO_AND_DELAY_INIT:	// called once during init phase of u8g2/u8x8
			/* No need to initialise the pin mode of SPI pins. */
			/* Initialise LCD Chip Select pin as output. */
			pinMode(LCD_CS, OUTPUT);
			pinMode(RESET, OUTPUT);
			break;							// can be used to setup pins
		case U8X8_MSG_DELAY_NANO:			// delay arg_int * 1 nano second
			tv.tv_sec = 0;
			tv.tv_nsec = (long) arg_int;
			nanosleep(&tv, NULL);
			break;
		case U8X8_MSG_DELAY_100NANO:		// delay arg_int * 100 nano seconds
			tv.tv_sec = 0;
			tv.tv_nsec = ((long) arg_int) * 100L;
			nanosleep(&tv, NULL);
			break;
		case U8X8_MSG_DELAY_10MICRO:		// delay arg_int * 10 micro seconds
			tv.tv_sec = 0;
			tv.tv_nsec = ((long) arg_int) * 10000L;
			nanosleep(&tv, NULL);
			break;
		case U8X8_MSG_DELAY_MILLI:			// delay arg_int * 1 milli second
			tv.tv_sec = 0;
			tv.tv_nsec = ((long) arg_int) * 1000000L;
			nanosleep(&tv, NULL);
			break;
		case U8X8_MSG_DELAY_I2C:				// arg_int is the I2C speed in 100KHz, e.g. 4 = 400 KHz
			break;							// arg_int=1: delay by 5us, arg_int = 4: delay by 1.25us
		case U8X8_MSG_GPIO_D0:				// D0 or SPI clock pin: Output level in arg_int
			//case U8X8_MSG_GPIO_SPI_CLOCK:
			break;
		case U8X8_MSG_GPIO_D1:				// D1 or SPI data pin: Output level in arg_int
			//case U8X8_MSG_GPIO_SPI_DATA:
			break;
		case U8X8_MSG_GPIO_D2:				// D2 pin: Output level in arg_int
			break;
		case U8X8_MSG_GPIO_D3:				// D3 pin: Output level in arg_int
			break;
		case U8X8_MSG_GPIO_D4:				// D4 pin: Output level in arg_int
			break;
		case U8X8_MSG_GPIO_D5:				// D5 pin: Output level in arg_int
			break;
		case U8X8_MSG_GPIO_D6:				// D6 pin: Output level in arg_int
			break;
		case U8X8_MSG_GPIO_D7:				// D7 pin: Output level in arg_int
			break;
		case U8X8_MSG_GPIO_E:				// E/WR pin: Output level in arg_int
			break;
		case U8X8_MSG_GPIO_CS:				// CS (chip select) pin: Output level in arg_int
			digitalWrite(LCD_CS, (int) arg_int);
			break;
		case U8X8_MSG_GPIO_DC:				// DC (data/cmd, A0, register select) pin: Output level in arg_int
			break;
		case U8X8_MSG_GPIO_RESET:			// Reset pin: Output level in arg_int
			digitalWrite(RESET, (int) arg_int);
			break;
		case U8X8_MSG_GPIO_CS1:				// CS1 (chip select) pin: Output level in arg_int
			break;
		case U8X8_MSG_GPIO_CS2:				// CS2 (chip select) pin: Output level in arg_int
			break;
		case U8X8_MSG_GPIO_I2C_CLOCK:		// arg_int=0: Output low at I2C clock pin
			break;							// arg_int=1: Input dir with pullup high for I2C clock pin
		case U8X8_MSG_GPIO_I2C_DATA:			// arg_int=0: Output low at I2C data pin
			break;							// arg_int=1: Input dir with pullup high for I2C data pin
		case U8X8_MSG_GPIO_MENU_SELECT:
			u8x8_SetGPIOResult(u8x8, /* get menu select pin state */ 0);
			break;
		case U8X8_MSG_GPIO_MENU_NEXT:
			u8x8_SetGPIOResult(u8x8, /* get menu next pin state */ 0);
			break;
		case U8X8_MSG_GPIO_MENU_PREV:
			u8x8_SetGPIOResult(u8x8, /* get menu prev pin state */ 0);
			break;
		case U8X8_MSG_GPIO_MENU_HOME:
			u8x8_SetGPIOResult(u8x8, /* get menu home pin state */ 0);
			break;
		default:
			u8x8_SetGPIOResult(u8x8, 1);			// default return value
			break;
	}
	return 1;
}


// TOUTES LES METHODES ICI SONT PREFIXES DE "SCREEN_"

void SCREEN_init(u8g2_t* u8g2) {

	/* Initialise graphics library. */
	u8g2_Setup_st7920_s_128x64_f(u8g2, U8G2_R2, u8x8_byte_rpi_hw_spi, u8x8_rpi_gpio_and_delay); // f(x) à mettre dans la librairie

	/* Send init sequence to the display, display will be in sleep mode. */
	u8g2_InitDisplay(u8g2);

	/* Wake up Display. */
	u8g2_SetPowerSave(u8g2, 0);

  u8g2_SetFont(u8g2, u8g2_font_profont10_mr);

  /****************************** INIT DONE *************************/
}

// Screens de menu / acceuils etc ...

void SCREEN_splashSreen(u8g2_t* u8g2) { // écran de boot

  u8g2_SetFontMode(u8g2, 0);
  u8g2_SetDrawColor(u8g2, 1); // blanc sur fond bleu

  u8g2_ClearBuffer(u8g2);
  u8g2_SetFont(u8g2, u8g2_font_8x13B_tf);
  u8g2_DrawXBM(u8g2, 29, 0, 69, 32, title);
  u8g2_DrawStr(u8g2, 20, 54, "IUT EDITION");
  int var = 0;
  for (size_t j = 0; j < 8; j++) {
    var++;
    SHIELD_printVies((Joueur){0, j});
    for (int i = 0; i < 64; i+=9) {
      u8g2_DrawXBM(u8g2, 0, i, 11, 8, alien_title[var%2]);
      u8g2_DrawXBM(u8g2, 116, i, 11, 8, alien_title[var++%2]);
    }
    u8g2_SendBuffer(u8g2);
  }
  u8g2_ClearBuffer(u8g2);
}

void SCREEN_menu(u8g2_t* u8g2) { // jouer, tableau des scores, crédits
  u8g2_SetFontMode(u8g2, 0);
  u8g2_SetDrawColor(u8g2, 1); // blanc sur fond bleu

  u8g2_SetFont(u8g2, u8g2_font_8x13B_tf);
  u8g2_DrawStr(u8g2, 12, 12, "Space Invader");
  u8g2_DrawHLine(u8g2, 0, 14, SCREEN_WIDTH);

  u8g2_SetFont(u8g2, u8g2_font_6x10_tf);
  u8g2_DrawStr(u8g2, 49, 27, "Jouer");
  u8g2_DrawStr(u8g2, 16, 39, "Meilleurs scores");
  u8g2_DrawUTF8(u8g2, 43, 51, "Crédits");
  u8g2_DrawStr(u8g2, 43, 63, "Quitter");

}

void SCREEN_tableauDesScores(u8g2_t* u8g2) {
  Score tabScores[NB_RECORDS];

  Score* addrTab = tabScores;
  lireRecords(&addrTab);

  u8g2_SetFontMode(u8g2, 0);
  u8g2_SetDrawColor(u8g2, 1); // blanc sur fond bleu
  u8g2_ClearBuffer(u8g2);

  u8g2_SetFont(u8g2, u8g2_font_8x13B_tf);
  u8g2_DrawUTF8(u8g2, 36, 12, "Records");
  u8g2_DrawHLine(u8g2, 0, 14, SCREEN_WIDTH);

  u8g2_SetFont(u8g2, u8g2_font_6x10_tf);

  for (int i = 0; i < NB_RECORDS; i++) {
    char strScore[10];
    sprintf(strScore, "%d", tabScores[i].score);
    u8g2_DrawUTF8(u8g2, 5, i*10 +23, tabScores[i].nom);
    u8g2_DrawUTF8(u8g2, 70, i*10 +23, strScore);
  }

  u8g2_SendBuffer(u8g2);
}

void SCREEN_credits(u8g2_t* u8g2) { // print nos noms
  u8g2_SetFontMode(u8g2, 0);
  u8g2_SetDrawColor(u8g2, 1); // blanc sur fond bleu
  u8g2_ClearBuffer(u8g2);

  u8g2_SetFont(u8g2, u8g2_font_8x13B_tf);
  u8g2_DrawUTF8(u8g2, 36, 12, "Crédits");
  u8g2_DrawHLine(u8g2, 0, 14, SCREEN_WIDTH);

  u8g2_SetFont(u8g2, u8g2_font_6x10_tf);
  u8g2_DrawUTF8(u8g2, 25, 39, "Gardes Dorian");
  u8g2_DrawUTF8(u8g2, 22, 51, "Rémi Pelletier");
  u8g2_SendBuffer(u8g2);

}

// Différents prints pour générer l'écran en cours de partie.

void SCREEN_printScore(u8g2_t* u8g2, unsigned int score) {

  char strScore[10];
  sprintf(strScore, "%d", score);

  u8g2_SetFontMode(u8g2, 0);
  u8g2_SetDrawColor(u8g2, 1); // blanc sur fond bleu
  u8g2_SetFont(u8g2, u8g2_font_4x6_mn);
  u8g2_DrawUTF8(u8g2, 0, 6, strScore);
}

void SCREEN_printEnnemis(u8g2_t* u8g2, Ennemi tabEnnemis[], int nbEnnemis) {
  u8g2_SetFontMode(u8g2, 1);
  u8g2_SetDrawColor(u8g2, 1); // blanc sur fond transparent
  for (int i = 0; i < nbEnnemis; i++) {
    int textureAleatoire = rand() % 2;
    switch (tabEnnemis[i].type) {
      case 3: //boss
        u8g2_DrawXBM(u8g2, tabEnnemis[i].position.pos_x, tabEnnemis[i].position.pos_y, BOSS_WIDTH, BOSS_EIGHT, textureBoss[textureAleatoire]);
        break;
      default:
        u8g2_DrawXBM(u8g2, tabEnnemis[i].position.pos_x, tabEnnemis[i].position.pos_y, ALIEN_WIDTH, ALIEN_EIGHT, textureAliens[tabEnnemis[i].type][textureAleatoire]);
        break;
    }
  }
}

void SCREEN_printJoueur(u8g2_t* u8g2, Joueur joueur) {
  u8g2_SetFontMode(u8g2, 1);
  u8g2_SetDrawColor(u8g2, 1); // blanc sur fond transparent
  u8g2_DrawXBM(u8g2, joueur.pos_x, JOUEUR_POS_Y, JOUEUR_WIDTH, JOUEUR_EIGHT, textureJoueur);
}

void SCREEN_printMissiles(u8g2_t* u8g2, Missile tabMissiles[], int nbMissiles) {
  u8g2_SetFontMode(u8g2, 1);
  u8g2_SetDrawColor(u8g2, 1); // blanc sur fond transparent
  for (int i = 0; i < nbMissiles; i++) {
    u8g2_DrawXBM(u8g2, tabMissiles[i].position.pos_x, tabMissiles[i].position.pos_y, MISSILE_WIDTH, MISSILE_EIGHT, textureMissile);
  }
}

void SCREEN_printExplosion(u8g2_t* u8g2, Position position) {
  u8g2_SetFontMode(u8g2, 1);
  u8g2_SetDrawColor(u8g2, 1); // blanc sur fond transparent
  u8g2_DrawXBM(u8g2, position.pos_x, position.pos_y, EXPLOSION_WIDTH, EXPLOSION_EIGHT, textureExplosion);
}

  // Fins de parties

void SCREEN_gameOver(u8g2_t* u8g2, unsigned int score) {
  u8g2_SetFontMode(u8g2, 0);
  u8g2_SetDrawColor(u8g2, 1); // blanc sur fond bleu

  u8g2_ClearBuffer(u8g2);

  u8g2_SetFont(u8g2, u8g2_font_freedoomr10_tu);
  u8g2_DrawStr(u8g2, SCREEN_WIDTH/2 - u8g2_GetStrWidth(u8g2, "GAME OVER")/2, SCREEN_EIGHT/2 +5, "GAME OVER");

  u8g2_DrawXBM(u8g2, SCREEN_WIDTH/2 - BOSS_WIDTH -5, SCREEN_EIGHT/2 -17, ALIEN_WIDTH, ALIEN_EIGHT, textureAliens[2][0]); // Dans l'ordre de G à D
  u8g2_DrawXBM(u8g2, SCREEN_WIDTH/2 - BOSS_WIDTH/2, SCREEN_EIGHT/2 -17, BOSS_WIDTH, BOSS_EIGHT, textureBoss[0]);
  u8g2_DrawXBM(u8g2, SCREEN_WIDTH/2 + BOSS_WIDTH/2 +3, SCREEN_EIGHT/2 -17, ALIEN_WIDTH, ALIEN_EIGHT, textureAliens[1][0]);
  u8g2_DrawHLine(u8g2, SCREEN_WIDTH/4, SCREEN_EIGHT/2 + 5, SCREEN_WIDTH/2); // Barre horizontale

  char strScore[10];
  sprintf(strScore, "%d", score);
  u8g2_DrawStr(u8g2, SCREEN_WIDTH/2 - u8g2_GetStrWidth(u8g2, strScore)/2, SCREEN_EIGHT/2 +20, strScore);

  u8g2_SendBuffer(u8g2);
}

void SCREEN_gameWin(u8g2_t* u8g2, unsigned int score) {
  u8g2_SetFontMode(u8g2, 0);
  u8g2_SetDrawColor(u8g2, 1); // blanc sur fond bleu

  u8g2_ClearBuffer(u8g2);

  u8g2_SetFont(u8g2, u8g2_font_freedoomr10_tu);
  u8g2_DrawStr(u8g2, SCREEN_WIDTH/2 - u8g2_GetStrWidth(u8g2, "GAME WIN")/2, SCREEN_EIGHT/2 +5, "GAME WIN");

  u8g2_DrawXBM(u8g2, SCREEN_WIDTH/2 - BOSS_WIDTH -5, SCREEN_EIGHT/2 -17, ALIEN_WIDTH, ALIEN_EIGHT, textureAliens[2][0]); // Dans l'ordre de G à D
  u8g2_DrawXBM(u8g2, SCREEN_WIDTH/2 - BOSS_WIDTH/2, SCREEN_EIGHT/2 -17, BOSS_WIDTH, BOSS_EIGHT, textureBoss[0]);
  u8g2_DrawXBM(u8g2, SCREEN_WIDTH/2 + BOSS_WIDTH/2 +3, SCREEN_EIGHT/2 -17, ALIEN_WIDTH, ALIEN_EIGHT, textureAliens[1][0]);
  u8g2_DrawHLine(u8g2, SCREEN_WIDTH/4, SCREEN_EIGHT/2 + 5, SCREEN_WIDTH/2); // Barre horizontale

  char strScore[10];
  sprintf(strScore, "%d", score);
  u8g2_DrawStr(u8g2, SCREEN_WIDTH/2 - u8g2_GetStrWidth(u8g2, strScore)/2, SCREEN_EIGHT/2 +20, strScore);

  u8g2_SendBuffer(u8g2);
}

void SCREEN_nouveauRecord(u8g2_t* u8g2) {

  u8g2_SetFontMode(u8g2, 0);
  u8g2_SetDrawColor(u8g2, 1); // blanc sur fond bleu

  u8g2_ClearBuffer(u8g2);

  u8g2_SetFont(u8g2, u8g2_font_8x13B_tf);
  u8g2_DrawUTF8(u8g2, 8, 12, "Nouveau record");

  u8g2_SetFont(u8g2, u8g2_font_open_iconic_arrow_2x_t); // Police allégée qui ne contient que des flèches
  u8g2_DrawGlyph(u8g2, 114, 35, 82);  // "➤"
  u8g2_DrawGlyph(u8g2, 114, 55, 82);

  u8g2_SetFont(u8g2, u8g2_font_6x10_tf);
  u8g2_DrawStr(u8g2, 4, 27, "Suivez les");
  u8g2_DrawUTF8(u8g2, 4, 38, "informations à");
  u8g2_DrawUTF8(u8g2, 4, 49, "l'écran pour");
  u8g2_DrawStr(u8g2, 4, 60, "l'enregistrer");

  u8g2_SendBuffer(u8g2);
}