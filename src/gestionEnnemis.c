#include "../defConstantesEtTypes.h"

#include <malloc.h>
#include <stdlib.h>


void descendreEnnemis(Ennemi* tabEnnemis[], int nbEnnemis, int valeurDescente) {
  for (int i = 0; i < nbEnnemis; i++) {
    (*tabEnnemis)[i].position.pos_y += valeurDescente;
  }
}

void testchangementDirectionEnnemis(Ennemi* tabEnnemis[], Direction* sensEnnemis, int nbEnnemis) {
  for (int i = 0; i < nbEnnemis; i++) { //balaye ennemis
    if ((*tabEnnemis)[i].position.pos_x == 0) { // test débordement gauche
      *sensEnnemis = DROITE;
      break; // On sort du for
    } else {
      switch ((*tabEnnemis)[i].type) { // test débordement à droite
        case 3: //BOSS
          if ((*tabEnnemis)[i].position.pos_x >= SCREEN_WIDTH - BOSS_WIDTH -1) {
            *sensEnnemis = GAUCHE;
            descendreEnnemis(tabEnnemis, nbEnnemis, 4);
            i = nbEnnemis; // On modifie i pour sortir sans aller plus loin
          }
          break;
        default:// alien classique
          if ((*tabEnnemis)[i].position.pos_x >= SCREEN_WIDTH - ALIEN_WIDTH -1) {
            *sensEnnemis = GAUCHE;
            descendreEnnemis(tabEnnemis, nbEnnemis, 1);
            i = nbEnnemis; // On modifie i pour sortir sans aller plus loin
          }
          break;
      }
    } // end switch
  } //end for
}

void deplacerEnnemis(Ennemi* tabEnnemis[], Direction* sensEnnemis, int nbEnnemis) { //déplace tous les ennemis

  testchangementDirectionEnnemis(tabEnnemis, sensEnnemis, nbEnnemis); // Change le sens si nécéssaire

  if (*sensEnnemis == DROITE) {
    for (int i = 0; i < nbEnnemis; i++) { (*tabEnnemis)[i].position.pos_x ++; }
  } else {
    for (int i = 0; i < nbEnnemis; i++) { (*tabEnnemis)[i].position.pos_x --; }
  }
}

void spawnEnnemis(Ennemi* tabEnnemis[], int* nbEnnemis, int nbPointsVie) { // remplis l'écran d'ennemis
  #define NB_SUR_LARGEUR 14 // MAX 16
  #define NB_SUR_HAUTEUR 4 // MAX 6


  *tabEnnemis = calloc (NB_SUR_HAUTEUR * NB_SUR_LARGEUR, sizeof(Ennemi));
  if (*tabEnnemis == NULL) {
    perror("Echec calloc au spawn des ennemis !") ;
    exit(EXIT_FAILURE) ;
  }

  *nbEnnemis = 0;
  for (int x = (SCREEN_WIDTH - NB_SUR_LARGEUR * ALIEN_WIDTH) / 2; x < NB_SUR_LARGEUR * ALIEN_WIDTH; x += ALIEN_WIDTH + 6) { // Balayage horizontal
    for (int y = 0; y < NB_SUR_HAUTEUR * ALIEN_EIGHT; y += ALIEN_EIGHT + 3) { // Balayage vertical
      (*tabEnnemis)[*nbEnnemis] = (Ennemi){rand() % 3, nbPointsVie, (Position){x, y}};
      (*nbEnnemis)++;

    }
  }
}

void spawnBOSS(Ennemi* tabEnnemis[], int* nbEnnemis) { // Niveau final

  *tabEnnemis = calloc (3, sizeof(Ennemi));
  if (*tabEnnemis == NULL) {
    perror("Echec calloc au spawn du boss !") ;
    exit (EXIT_FAILURE) ;
  }
  *nbEnnemis = 3;

  (*tabEnnemis)[0] = (Ennemi){3, NB_PTS_VIE_BOSS, (Position){36, 4}};
  (*tabEnnemis)[1] = (Ennemi){3, NB_PTS_VIE_BOSS, (Position){58, 18}};
  (*tabEnnemis)[2] = (Ennemi){3, NB_PTS_VIE_BOSS, (Position){80, 4}};

}

void retirerEnnemi(Ennemi* tabEnnemis[], int* nbEnnemis, int indiceARetirer) {

  for (int i = indiceARetirer; i < *nbEnnemis-1; i++) { // nbEnnemis - 1 pour éviter de sortir du tableau
    (*tabEnnemis)[i] = (*tabEnnemis)[i+1];
  }

  (*nbEnnemis) --;
  *tabEnnemis = realloc(*tabEnnemis, *nbEnnemis * sizeof(Ennemi));
}