#include "../defConstantesEtTypes.h"

#include <stdio.h>

void deplacerJoueur(Joueur* joueur, Direction direction){

  if (direction == DROITE) { // && joueur->pos_x < SCREEN_WIDTH - JOUEUR_WIDTH -1) { // -1 car pos_x commence à 0
    if ((int) joueur->pos_x +4 <= SCREEN_WIDTH -JOUEUR_WIDTH -1) joueur->pos_x += 4; // 4 car il est 4x plus rapide que les ennemis
    else joueur->pos_x = SCREEN_WIDTH -JOUEUR_WIDTH -1;
  } else if (direction == GAUCHE) { // && joueur->pos_x < SCREEN_WIDTH - JOUEUR_WIDTH -1) {
    if ((int) joueur->pos_x -4 > 0) joueur->pos_x -= 4; // 4 car il est 4x plus rapide que les ennemis
    else joueur->pos_x = 0;
  }
}