#include "../defConstantesEtTypes.h"

#include <malloc.h>
#include <stdlib.h>
#include "../headers/gestionEcran.h"
#include "../headers/u8g2.h"
#include "../headers/gestionEnnemis.h"


// Fonctions tirer... prépare le missile pour l'ajouter au tableau avec nouveauMissile()
Missile tirerEnnemi(Ennemi tabEnnemis[], int nbEnnemis) {

  int colonne_choisie = tabEnnemis[(rand() % nbEnnemis)].position.pos_x;  // ATTENTION AU MODULO 0
  int indice_ennemi_plus_bas_trouve = 0;

  for (int i = 0; i < nbEnnemis; i++) {
    if (tabEnnemis[i].position.pos_x == colonne_choisie) { // double if -> moche mais plus compréhensible
      if (tabEnnemis[i].position.pos_y > tabEnnemis[indice_ennemi_plus_bas_trouve].position.pos_x) {
        indice_ennemi_plus_bas_trouve = i;
      }
    }
  }

  Missile nouveauMissile;
  nouveauMissile.sens = HOSTILE;
  if (tabEnnemis[indice_ennemi_plus_bas_trouve].type == 3) { // Si boss
    nouveauMissile.position.pos_x = tabEnnemis[indice_ennemi_plus_bas_trouve].position.pos_x + BOSS_WIDTH /2 - MISSILE_WIDTH /2;
    nouveauMissile.position.pos_y = tabEnnemis[indice_ennemi_plus_bas_trouve].position.pos_y + BOSS_EIGHT;
  } else { // Si autre
    nouveauMissile.position.pos_x = tabEnnemis[indice_ennemi_plus_bas_trouve].position.pos_x + ALIEN_WIDTH /2 - MISSILE_WIDTH /2;
    nouveauMissile.position.pos_y = tabEnnemis[indice_ennemi_plus_bas_trouve].position.pos_y + ALIEN_EIGHT;
  }

  return nouveauMissile;
}

Missile tirerJoueur(Joueur joueur) {
  Missile nouveauMissile;
  nouveauMissile.sens = ALLIE;

  nouveauMissile.position.pos_x = joueur.pos_x + JOUEUR_WIDTH /2 - MISSILE_WIDTH /2;
  nouveauMissile.position.pos_y = JOUEUR_POS_Y - MISSILE_EIGHT;

  return nouveauMissile;
}

void nouveauMissile( Missile* tabMissiles[], int* nbMissiles, Missile nouveauMissile) {
  (*nbMissiles) ++;
  *tabMissiles = realloc(*tabMissiles, (*nbMissiles) * sizeof(Missile)); // +1 case
  (*tabMissiles)[(*nbMissiles)-1] = nouveauMissile;
}

void retirerMissile(Missile* tabMissiles[], int* nbMissiles, int indiceARetirer) {

  for (int i = indiceARetirer; i < *nbMissiles-1; i++) { // nbMissiles - 1 pour éviter de sortir du tableau
    (*tabMissiles)[i] = (*tabMissiles)[i+1];
  }

  (*nbMissiles) --;
  *tabMissiles = realloc(*tabMissiles, (*nbMissiles) * sizeof(Missile));
}

void deplacerMissiles(Missile* tabMissiles[], int nbMissiles) { // Déplace tous les missiles

  for (int i = 0; i < nbMissiles; i++) {
    if ((*tabMissiles)[i].sens == HOSTILE) (*tabMissiles)[i].position.pos_y += VITESSE_MISSILES;
    else (*tabMissiles)[i].position.pos_y -= VITESSE_MISSILES;
  }
}

// retire les missiles, Missiles, vies si colisions
void checkColisions(u8g2_t* u8g2, Missile* tabMissiles[], int* nbMissiles, Ennemi* tabEnnemis[], int* nbEnnemis, Joueur* joueur) {

  for (int i = 0; i < *nbMissiles; i++) { // for des missiles
    if ((*tabMissiles)[i].sens == HOSTILE) { // On teste avec le joueur et le bord de l'écran

      if ((*tabMissiles)[i].position.pos_y >= SCREEN_EIGHT - MISSILE_EIGHT -1) retirerMissile(tabMissiles, nbMissiles, i); // Sors de l'écran
      else if ( (*tabMissiles)[i].position.pos_y + MISSILE_EIGHT >= JOUEUR_POS_Y // Test hauteur
            && (*tabMissiles)[i].position.pos_x < joueur->pos_x + JOUEUR_WIDTH // Test droite
            && (*tabMissiles)[i].position.pos_x > joueur->pos_x - MISSILE_WIDTH /*Test gauche*/) { // formule magique pour comparer les hitboxs qui dit oui quand colision
        retirerMissile(tabMissiles, nbMissiles, i);
        joueur->ptsVie --;
        digitalWrite(LED_R, HIGH);
        if (joueur->ptsVie <= 0) SCREEN_printExplosion(u8g2, (Position){joueur->pos_x, JOUEUR_POS_Y});
      }

    } else { // On teste tous les ennemis et le bord de l'écran

      if ((int) (*tabMissiles)[i].position.pos_y < 0)retirerMissile(tabMissiles, nbMissiles, i); // Sors de l'écran
      else { //teste tous les ennemis
        for (int j = 0; j < *nbEnnemis; j++) { // for des ennemis
          if ( (*tabEnnemis)[j].type == 3 ) { // Sépare BOSS des autres
            if ( (*tabEnnemis)[j].position.pos_y + BOSS_EIGHT >= (*tabMissiles)[i].position.pos_y // Test hauteur
             && (*tabMissiles)[i].position.pos_x < (*tabEnnemis)[j].position.pos_x + BOSS_WIDTH // Test droite
             && (*tabMissiles)[i].position.pos_x > (*tabEnnemis)[j].position.pos_x - MISSILE_WIDTH /*TestGauche*/) { // formule magique pour comparer les hitboxs qui dit oui quand colision
              retirerMissile(tabMissiles, nbMissiles, i);
              (*tabEnnemis)[j].ptsVie --;
              digitalWrite(LED_V, HIGH);
              if ((*tabEnnemis)[j].ptsVie <= 0) {
                SCREEN_printExplosion(u8g2, (*tabEnnemis)[j].position);
                retirerEnnemi(tabEnnemis, nbEnnemis, j);
              }
              break; // pas besoin de vérifier les autres ennemis
            }
          } else /*nonboss*/ if ( (*tabEnnemis)[j].position.pos_y + ALIEN_EIGHT >= (*tabMissiles)[i].position.pos_y // Test hauteur
           && (*tabMissiles)[i].position.pos_x < (*tabEnnemis)[j].position.pos_x + ALIEN_WIDTH // Test droite
           && (*tabMissiles)[i].position.pos_x > (*tabEnnemis)[j].position.pos_x - MISSILE_WIDTH /*TestGauche*/) { // formule magique pour comparer les hitboxs qui dit oui quand colision
            retirerMissile(tabMissiles, nbMissiles, i);
            (*tabEnnemis)[j].ptsVie --;
            digitalWrite(LED_V, HIGH);
            if ((*tabEnnemis)[j].ptsVie <= 0) {
              SCREEN_printExplosion(u8g2, (*tabEnnemis)[j].position);
              retirerEnnemi(tabEnnemis, nbEnnemis, j);
            }
            break; // pas besoin de vérifier les autres ennemis
          } // end if non boss
        } //end for : ennemis

      }
    }
  } //end for : missiles
}
