#include "../defConstantesEtTypes.h"

#include <wiringPi.h>
#include <stdio.h>

const int BARGRAPH[8] = { 4, 17, 18, 27, 22, 23, 24, 25};


// TOUTES LES METHODES ICI SONT PREFIXES DE "SHIELD_"
void SHIELD_init() {
  // Init wiringPi.
  wiringPiSetupGpio();

  pinMode(BTN_D, INPUT);
  pinMode(BTN_G, INPUT);
  pinMode(LED_R, OUTPUT);
  pinMode(LED_V, OUTPUT);
  for (int i = 0; i < 8; i++) {
    pinMode(BARGRAPH[i], OUTPUT);
  }
}

void SHIELD_finPartie() { //ancien sinBARGRAPH

  digitalWrite(LED_R, LOW);
  digitalWrite(LED_V, HIGH);

  for (int i = 0; i < 30; i++) {
    for (int j = 0; j < 8; j++) {
      digitalWrite(BARGRAPH[j], (i + j) % 2); // LOW = 0 et HIGH = 1 (cf wiringPi.h)
    }
    digitalWrite(LED_R, !digitalRead(LED_R));
    digitalWrite(LED_V, !digitalRead(LED_V));
    delay(100); // MOCHE !
  }
}

void SHIELD_printVies(Joueur joueur) {
  for (int i = 0; i < 8; i++) digitalWrite(BARGRAPH[i], LOW); // On eteind tout
  for (int i = 0; i < joueur.ptsVie; i++) digitalWrite(BARGRAPH[i], HIGH); // On allume
}

Direction SHIELD_readBTNS(){

  if (digitalRead(BTN_D) == HIGH && digitalRead(BTN_G) == LOW) return DROITE;
  else if (digitalRead(BTN_D) == LOW && digitalRead(BTN_G) == HIGH) return GAUCHE;
  else return AUTRE;
}

void SHIELD_shutdown() {
  SHIELD_printVies((Joueur){0, 0});
  digitalWrite(LED_V, LOW);
  digitalWrite(LED_R, LOW);
}